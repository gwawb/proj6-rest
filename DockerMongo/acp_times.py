"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    segment = [200.0, 200.0, 200.0, 400.0, 300.0]
    i = 0
    shift = 0.0
    speed = 34.0
    time = arrow.get(brevet_start_time)
    dist = float(min(brevet_dist_km, control_dist_km))
    while dist > 0:
        if dist > segment[i]:
            shift += segment[i]/speed
            dist -= segment[i]
            speed -= 2
            i += 1
        else:
            shift += dist/speed
            dist = 0
    shift *= 60
    time = time.shift(minutes=round(shift))
    return time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    segment = [60.0, 140.0, 200.0, 200.0, 400.0, 300.0]
    speed = [20.0, 15.0, 15.0, 15.0, 11.428, 13.333]
    i = 0
    shift = 0.0
    time = arrow.get(brevet_start_time)
    dist = float(min(brevet_dist_km, control_dist_km))
    while True:
        if dist > segment[i]:
            shift += segment[i]/speed[i]
            dist -= segment[i]
            if i == 0:
                shift += 1
            i += 1
        else:
            shift += dist/speed[i]
            if i == 0:
                shift += 1
            dist = 0
        if dist == 0:
            break
    shift *= 60

    time = time.shift(minutes=round(shift))
    return time.isoformat()
