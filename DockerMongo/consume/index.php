<html>
    <head>
        <title>CIS 322 REST-api: Controle list</title>
    </head>

    <body>
        <h1>List of controles</h1>
        <form action="/index.php" method="get">
          <label for="list">list:</label>
          <select id='list' name="list">
            <option value="listAll">List All</option>
            <option value="listOpenOnly">Open Only</option>
            <option value="listCloseOnly">Close Only</option>
          </select>
          <label for="format">format:</label>
          <select id='format' name="format">
            <option value="json">JSON</option>
            <option value="csv">csv</option>
          </select>
          <label for="number">number:</label>
          <select id='number' name="number">
            <option value="">All</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
            <option value="13">13</option>
            <option value="14">14</option>
            <option value="15">15</option>
            <option value="16">16</option>
            <option value="17">17</option>
            <option value="18">18</option>
            <option value="19">19</option>
            <option value="20">20</option>
          </select>
          <input type="submit" value="Submit">
        </form>
        <ul>
            <?php
            /* . $_SERVER['REQUEST_URI']*/
            $json = file_get_contents("http://laptop-service/" . $_GET['list'] . "/" . $_GET['format'] . "?top=" . $_GET['number']);
            if ($_GET['format'] == 'csv') {
              $obj = json_decode($json);
              echo "$obj";
            }
            else {
              $obj = json_decode($json);
	             $time = $obj->times;
               foreach ($time as $o) {
                 echo "<li>$o</li>";
               }
            }
            //echo "<p>$GET</p>";
            ?>
        </ul>
    </body>
</html>
