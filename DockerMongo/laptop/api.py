# Laptop Service

import os
import csv
from flask import Flask, request, make_response
from flask_restful import Resource, Api
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

class Laptop(Resource):
    def get(self, param = 'listAll', format = 'json'):
        number = request.args.get('top', '')
        #format = request.path
        _items = db.tododb.find()
        items = [item for item in _items]

        if format == 'csv':
            ret = ''
        else:
            ret = []

        if number == '':
            number = '20'

        c = ''
        i = int(number)
        for item in items:
            if i == 0:
                break
            if format == 'csv':
                if param == 'listOpenOnly':
                    ret = ret + c + item["open"]
                elif param == 'listCloseOnly':
                    ret = ret + c + item["close"]
                else:
                    ret = ret + c + item["open"] + ',' + item["close"]
            else:
                if param == 'listOpenOnly':
                    ret.append(item["open"])
                elif param == 'listCloseOnly':
                    ret.append(item["close"])
                else:
                    ret.append(item["open"] + ", " + item["close"])
            i -= 1
            c = ','

        if format == 'csv':
            return ret
        else:
            return {
                'times': ret
                }

# Create routes
# Another way, without decorators
#api.add_resource(Laptop, '/')
api.add_resource(Laptop, '/', '/<path:param>','/<path:param>/<path:format>')
#api.add_resource(Laptop, '/<string:todo_id>')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
